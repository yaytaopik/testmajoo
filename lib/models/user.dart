class User {
  String userName;
  String email;
  String password;

  User(text, Emailtext, Passwordtext, { this.userName, this.email, this.password});

  User.fromJson(Map<String, dynamic> json)
      : userName = json['username'],
        email = json['email'],
        password = json['password'];

  Map<String, dynamic> toJson() =>
      {'username': userName, 'email': email, 'password': password};
}

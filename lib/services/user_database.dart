import 'dart:io';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';
import 'package:majootestcase/models/user.dart';

class UserDatabase {
  static String path;
  static final _databaseName = "mydb.db";
  static final _databaseVersion = 1;

  static final _table_user = 'users';
  static final _table_logins = 'logins';

  UserDatabase._privateConstructor();
  static final UserDatabase instance = UserDatabase._privateConstructor();
   // only have a single app-wide reference to the database
  static Database _database;

  Future get database async {
    if (_database != null) return _database;
    _database = await _initDatabase();
    return _database;
  }

  _initDatabase() async {
    Directory documentsDirectory = await getApplicationSupportDirectory();
    String path = join(documentsDirectory.path, _databaseName);
    return await openDatabase(path, version:_databaseVersion, onCreate: _onCreate);
  }

  Future _onCreate(Database db, int version) async {
     await db.execute(
      "CREATE TABLE users(username TEXT, email TEXT, password TEXT)",
    );
     await db.execute(
      "CREATE TABLE logins(username TEXT, email TEXT, password TEXT)",
    );
  }

  static Future getFileData() async {
     return getDatabasesPath().then((s){
       return path=s;
     });
   }

     Future insertUser(User user) async{
     Database db = await instance.database;

   var users=await  db.rawQuery("select * from users where email = "+user.email);
     if(users.length>0)
       {
         return -1;
       }
       return await db.insert("users", user.toJson(),conflictAlgorithm: ConflictAlgorithm.ignore);
   }

  Future checkUserLogin(String email, String password) async
   {
     Database db = await instance.database;
     var res=await  db.rawQuery("select * from users where email = '$email' and password = '$password'");
     if(res.length>0)
       {
         List list =
         res.toList().map((c) => User.fromJson(c)).toList() ;

         print("Data "+list.toString());
         await  db.insert("logins",list[0].toUserMap());
         return list[0];
       }
       return null;
   }

    Future getUser() async{
     Database db = await instance.database;
    var logins=await  db.rawQuery("select * from logins");
    if(logins==null)
      return 0;
    return logins.length;

   }

  Future getUserData() async{
    Database db = await instance.database;
    var res=await  db.rawQuery("select * from logins");
    print("result user data $res");
    print("result user data "+res.toString());
    List list =
    res.toList().map((c) => User.fromJson(c)).toList() ;
    return list[0];

  }

  Future deleteUser(String email) async{
    Database db = await instance.database;
   var logins= db.delete(_table_logins, where: "email = ?", whereArgs: [email]);
      return logins;

  }

}

